#!/usr/bin/env python
# coding: utf-8

# # Numpy Assignment Intellipaat

# In[32]:


import sys
get_ipython().system('{sys.executable} -m pip install numpy')
'pip install --upgrade pip'


# In[2]:


import numpy as np
a = np.array([1,2,3])
print(a)


# In[7]:


#Q.1:
array1 = np.full((5,5),0)
print(array1)


# In[6]:


array1_1 = np.zeros([5,5])
print(array1_1)


# In[23]:


#Q.2:
a1 = np.arange(10,105,5)
print(a1)


# In[33]:


#Q.3:
a3 = np.array([[10,20,30],[40,50,60]])
print(a3)
print("The shape of the array is:",np.shape(a3))
print("The size of the array is:",np.size(a3))
print("The data type of array is:",a3.dtype)


# In[24]:


a3_1 = np.array([10,20,30,40,50,60])
a3_1.shape = 3,2
print(a3_1)
print("The data type of the array a3_1 is:", a3_1.dtype)


# In[43]:


#Q.4:
a4_1 = np.array([1,2,3])
a4_2 = np.array([4,5,6])
a4_sum = np.sum([a4_1,a4_2])
print("The addition value of two arrays is:", a4_sum)

a4_sub = np.subtract(a4_1,a4_2)
print("The subtraction value of two arrays is:", a4_sub)

a4_multiply = np.multiply(a4_1,a4_2)
print("The multiplication value of two arrays is:", a4_multiply)

a4_division = np.divide(a4_1,a4_2)
print("The division value of two arrays is:", a4_division)


# In[30]:


#Q.5:
a5 = np.array([[10,20,30],[40,50,60],[70,80,90]])
print(a5)


# In[36]:


#Q.5a:
# print("The third column of the array is:", np.hsplit(a5,np.array([3])))

print("The third column of the array is:", a5[0:,2:])

#Q.5b:
print("The second row of the array is:", a5[1:2])

#Q.5c:
print("The cells at intersection of first two rows and first two columns:", a5[:2,:2])


# In[21]:


#Q.6:
a6 = np.arange(10,100,10)
print(a6)


# In[27]:


a6.shape = 3,3
print(a6)


# In[31]:


a5_1 = np.array([[10,20,30],[40,50,60],[70,80,90]])
print(a5_1)


# In[33]:


print("The third column of the array is:", a5[0:,2:])


# In[42]:


a = 2700
b = 1500

while b <= a:
    if b%7 == 0 and b%5 == 0:
        print("The value is divide by 7 and multiplication of 5:", b)
        b = b+1
    else:
        b = b+1


# In[43]:


np.reciprocal(a5_1)


# In[51]:


a7 = np.array([[10,20,30],[40,50,60]])
print(a7)

a7_1 = a7[::-1]
print(a7_1)


# In[59]:


list_1 = [1,2,3,4,5,6]
print(list_1[-4:])
print(list_1[::-1])


# In[61]:


ap = np.identity(3)
print("3x3 matrix:", ap)


# In[48]:


import numpy as np
with open("Desktop//IQ_scores.txt") as fi:
    file = np.loadtxt(fi,unpack = True, skiprows=1, delimiter="\t")
    print(file)
    
    fi.close()


# In[2]:


import numpy as np
dtype = [('IQ', int),('Brain',float),('Height', float),('Weight', int)]
values = [(147,85.65,70.5,155),(90,87.89,66,146),(96,86.54,68,135),(120,85.22,68.5,127)]
a = np.array(values, dtype=dtype)
np.sort(a, order = 'IQ')


# In[4]:


import numpy as np
dtype = [('IQ', int),('Brain',float),('Height', float),('Weight', int)]
with open("Desktop//IQ_scores.txt") as fi:
    file = np.loadtxt(fi,unpack = True, skiprows=1, delimiter="\t")
a = np.array(file, dtype=dtype)
np.sort(a, order = 'IQ')


# In[8]:


x = np.loadtxt(r"Desktop//IQ_scores.txt", skiprows =1, delimiter = None)
print(x)


# In[9]:


y = x[x[0:,0].argsort()]
print(y)


# In[10]:


z = dict(enumerate(y.flatten[],0))
print(z)


# In[19]:


z = np.average(y[0:, 1:],axis=1)
print(z)


# In[20]:


import numpy as np
z1 = np.median(y[0:,1:],axis=1)
print(z1)


# In[39]:


mydict1 = dict(zip(y[:,0],z1[0:]))
print(mydict1)


# In[42]:


mydict = dict(zip(y[:,0],z[0:]))
print(mydict)


# In[53]:


keys = y[:, 0]
values = zip(z,z1)
dictionary = dict(zip(keys,values))
print(dictionary)


# In[48]:


from copy import deepcopy
def dict_of_dicts_merge(a,b):
    overlapping_keys = a.keys() & b.keys()
    for key in overlapping_keys:
        c[key] = dict_of_dicts_merge(a[key],b[key])
    for key in a.keys() - overlapping_keys:
        c[key] = deepcopy(a[key])
    for key in b.keys() - overlapping_keys:
        c[key] = deepcopy(b[key])
    return c


# In[ ]:


d = dict()
y = []
for z in list:
    y = 


# In[73]:


from collections import defaultdict

test = y[0:6]

dic = {}
for x in test:
    val = x[0]
    if(val in dic.keys()): 
        print('key exist')
        dic[val].append(x[1:])
    else:
        dic[val] = x[1:]

print(dic)


# In[133]:


dic = dict()
test = y[0:6]

for x in test:
    key = x[0]
    if(key in dic.keys()):
        value = dic[key]
        dic[key] = list(value)+ (x[1:])
        print(dic)
    else:
        dic[key] = x[1:]
        

# print(dic)


# In[132]:


# ll = []
ll_1 = [1,2,3]
ll_1 += [56]
print(ll_1)


# In[30]:


import numpy as np
a = np.arange(30)
print(a)
a.shape = (5,6)
print(a)


# In[25]:


a1 = np.arange(40)
print(a1)
b1 = a1.reshape(8,5)
print(b1)
b2 = a1.reshape(4,10)
print(b2)


# In[ ]:




